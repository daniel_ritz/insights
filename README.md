# Insights

*Insights is under heavy alpha development!*

Insights is an open-source app to visualise data in a familiar spreadsheet-like way.
However, Insights focuses on strictly typed structured data and does not allow free-form spreadsheets.
