pub mod datatypes;
pub mod interfaces;

pub use datatypes::{Object, Interface, InterfaceDeclaration};