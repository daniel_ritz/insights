use crate::{datatypes::Integer, Interface, InterfaceDeclaration, Object};

pub struct Concatenable(Object);
impl Interface for Concatenable {
    fn required_methods() -> InterfaceDeclaration {
        InterfaceDeclaration {
            unary_methods: vec!["count"],
            binary_methods: vec!["concat"],
        }
    }

    fn use_(object: Object) -> Self {
        Self(object)
    }

    fn release(self) -> Object {
        self.0
    }
}
impl Concatenable {
    pub fn count(&self) -> i64 {
        let count = self.0.dispatch_unary("count").expect("No method 'count'");
        count.unbox::<Integer>().value()
    }

    pub fn concat(&self, other: &Object) -> Object {
        self.0.dispatch_binary("concat", other).expect("No method 'concat'")
    }
}