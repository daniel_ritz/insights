use crate::datatypes::{Boolean, Integer};

use crate::{Interface, InterfaceDeclaration, Object};

pub struct Collection(Object);
impl Interface for Collection {
    fn required_methods() -> InterfaceDeclaration {
        InterfaceDeclaration {
            unary_methods: vec!["count"],
            binary_methods: vec!["append"],
        }
    }
    
    fn use_(object: Object) -> Self {
        Self(object)
    }

    fn release(self) -> Object {
        self.0
    }
}
impl Collection {
    pub fn count(&self) -> i64 {
        let count = self.0.dispatch_unary("count").unwrap();
        count.unbox::<Integer>().value()
    }
    pub fn append(&self, item: &Object) -> bool {
        let result = self.0.dispatch_binary("append", item).unwrap();
        result.unbox::<Boolean>().value()
    }
}
