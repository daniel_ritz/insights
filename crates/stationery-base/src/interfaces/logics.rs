use crate::{Interface, InterfaceDeclaration, Object};

pub struct Logics(Object);
impl Interface for Logics {
    fn required_methods() -> InterfaceDeclaration {
        InterfaceDeclaration {
            unary_methods: vec!["not"],
            binary_methods: vec!["and", "or"],
        }
    }

    fn use_(object: Object) -> Self {
        Self(object)
    }

    fn release(self) -> Object {
        self.0
    }
}
impl Logics {
    pub fn and(&self, rhs: &Object) -> Object {
        self.0.dispatch_binary("and", rhs).unwrap()
    }
    pub fn or(&self, rhs: &Object) -> Object {
        self.0.dispatch_binary("or", rhs).unwrap()
    }
    pub fn not(&self) -> Object {
        self.0.dispatch_unary("not").unwrap()
    }
}
