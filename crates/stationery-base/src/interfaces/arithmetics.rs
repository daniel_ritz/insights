use crate::{Interface, InterfaceDeclaration, Object};

pub struct Arithmetics(Object);
impl Interface for Arithmetics {
    fn required_methods() -> InterfaceDeclaration {
        InterfaceDeclaration {
            unary_methods: vec![],
            binary_methods: vec!["add", "subtract", "multiply", "divide"],
        }
    }

    fn use_(object: crate::Object) -> Self {
        Self(object)
    }

    fn release(self) -> crate::Object {
        self.0
    }
}

impl Arithmetics {
    pub fn add(&self, rhs: &Object) -> Object {
        self.0.dispatch_binary("add", rhs).unwrap()
    }
    pub fn subtract(&self, rhs: &Object) -> Object {
        self.0.dispatch_binary("subtract", rhs).unwrap()
    }
    pub fn multiply(&self, rhs: &Object) -> Object {
        self.0.dispatch_binary("multiply", rhs).unwrap()
    }
    pub fn divide(&self, rhs: &Object) -> Object {
        self.0.dispatch_binary("divide", rhs).unwrap()
    }
}
