use std::{cell::RefCell, collections::HashMap, fmt::Display};

use crate::{virt, Object};

use super::{dynamic_dispatch::Method, Datatype, VTable, Void};

struct Record {
    subtypes: Vec<(String, Datatype)>,
    fields: RefCell<HashMap<String, Object>>,
}

impl Record {
    pub fn type_(subtypes: Vec<(String, Datatype)>) -> Datatype {
        Datatype::Struct {
            name: Box::new("record".to_string()),
            subtypes,
            vtable: Some(vtable()),
        }
    }

    fn new(subtypes: Vec<(String, Datatype)>, values: HashMap<String, Object>) -> Object {
        let record = Self {
            subtypes: subtypes.clone(),
            fields: RefCell::new(values),
        };
        Object::new(Self::type_(subtypes), Box::new(record))
    }

    pub fn empty(subtypes: Vec<(String, Datatype)>) -> Object {
        let values = Self::generate_defaults(&subtypes);
        Self::new(subtypes, values)
    }

    pub fn builder() -> RecordTypeBuilder {
        RecordTypeBuilder::new()
    }

    fn generate_defaults(subtypes: &[(String, Datatype)]) -> HashMap<String, Object> {
        let mut map = HashMap::new();
        subtypes.iter().for_each(|subtype| {
            let value = subtype.1.default();
            map.insert(subtype.0.clone(), value);
        });
        map
    }

    pub fn get(&self, field: &str) -> Object {
        self.fields
            .borrow()
            .get(field)
            .cloned()
            .unwrap_or_else(Void::new)
    }

    pub fn set(&self, field: &str, value: Object) -> Result<(), ()> {
        if self.type_of_field(field) != *value.type_() {
            return Err(());
        }
        self.fields.borrow_mut().insert(field.to_string(), value);
        Ok(())
    }

    fn type_of_field(&self, field: &str) -> Datatype {
        let subtype = self.subtypes.iter().find(|subtype| subtype.0 == field);
        subtype
            .map(|subtype| subtype.1.clone())
            .unwrap_or(Void::type_())
    }

    fn clone(&self) -> Object {
        let record = Self {
            subtypes: self.subtypes.clone(),
            fields: self.fields.clone(),
        };
        Object::new(Self::type_(self.subtypes.clone()), Box::new(record))
    }
}

impl Display for Record {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let fields: Vec<String> = self
            .subtypes
            .iter()
            .map(|subtype| {
                let value = self
                    .fields
                    .borrow()
                    .get(&subtype.0)
                    .map(|value| value.to_string())
                    .unwrap_or("<void>".to_string());
                format!("{}: {}", subtype.0, value)
            })
            .collect();
        write!(f, "{{ {} }}", fields.join(", "))
    }
}

impl PartialEq for Record {
    fn eq(&self, other: &Self) -> bool {
        self.subtypes == other.subtypes && self.fields == other.fields
    }
}

struct RecordBuilder {
    fields: Vec<(String, Datatype)>,
    values: HashMap<String, Object>,
}
impl RecordBuilder {
    fn new(fields: Vec<(String, Datatype)>) -> Self {
        Self {
            fields,
            values: HashMap::new(),
        }
    }
    pub fn set(mut self, field: &str, value: Object) -> Self {
        if self.type_of_field(field) == *value.type_() {
            self.values.insert(field.to_string(), value);
        }
        self
    }
    fn type_of_field(&self, field: &str) -> Datatype {
        let subtype = self.fields.iter().find(|subtype| subtype.0 == field);
        subtype
            .map(|subtype| subtype.1.clone())
            .unwrap_or(Void::type_())
    }
    fn build(self) -> Object {
        Record::new(self.fields, self.values)
    }
}

#[derive(Clone)]
struct RecordTypeBuilder {
    fields: Vec<(String, Datatype)>,
}
impl RecordTypeBuilder {
    fn new() -> Self {
        Self { fields: vec![] }
    }
    pub fn field(mut self, name: &str, type_: Datatype) -> Self {
        self.fields.push((name.to_string(), type_));
        self
    }
    pub fn build(&self) -> Datatype {
        Datatype::Struct {
            name: Box::new("record".to_string()),
            subtypes: self.fields.clone(),
            vtable: Some(vtable()),
        }
    }
    pub fn record(&self) -> RecordBuilder {
        RecordBuilder::new(self.fields.clone())
    }
}

fn vtable() -> VTable {
    VTable::builder()
        .type_method("default", Method::nullary("record<>", default))
        .unary_method("clone", Method::unary("record<?>", clone))
        .binary_method("equals", Method::binary("?", "boolean", equals))
        .unary_method("print", Method::unary("text", print))
        .build()
}

virt! {
    default for Record: Record::empty(vec![]);
    fn clone for Record;
    fn equals for Record;
    fn print for Record;
}

#[cfg(test)]
mod tests {
    use crate::datatypes::{Integer, Text, Void};

    use super::{Record, RecordTypeBuilder};

    #[test]
    fn display() {
        let subtypes = vec![
            ("a".to_string(), Integer::type_()),
            ("b".to_string(), Text::type_()),
        ];
        let record = Record::empty(subtypes);
        assert_eq!("{ a: 0, b:  }", record.to_string());
    }

    #[test]
    fn set() {
        let subtypes = vec![
            ("a".to_string(), Integer::type_()),
            ("b".to_string(), Text::type_()),
        ];
        let record = Record::empty(subtypes);
        let record = record.unbox::<Record>();
        let _ = record.set("a", Integer::new(20));
        let _ = record.set("b", Text::new("hello"));
        assert_eq!("{ a: 20, b: hello }", record.to_string());
    }

    #[test]
    fn get() {
        let subtypes = vec![
            ("a".to_string(), Integer::type_()),
            ("b".to_string(), Text::type_()),
        ];
        let record = Record::empty(subtypes);
        let record = record.unbox::<Record>();
        let _ = record.set("a", Integer::new(20));
        let _ = record.set("b", Text::new("hello"));
        assert_eq!(Integer::new(20), record.get("a"));
        assert_eq!(Text::new("hello"), record.get("b"));
    }

    #[test]
    fn get_undefined() {
        let subtypes = vec![];
        let record = Record::empty(subtypes);
        let record = record.unbox::<Record>();
        assert_eq!(Void::new(), record.get("undefined"));
    }

    #[test]
    fn build_record_type() {
        let datatype = Record::builder()
            .field("a", Integer::type_())
            .field("b", Text::type_())
            .build();
        assert_eq!("record<a:integer,b:text>", datatype.to_string())
    }

    #[test]
    fn build_record() {
        let record = Record::builder()
            .field("a", Integer::type_())
            .field("b", Text::type_())
            .record()
            .set("a", Integer::new(42))
            .set("b", Text::new("hello"))
            .build();
        assert_eq!("{ a: 42, b: hello }", record.to_string())
    }
}
