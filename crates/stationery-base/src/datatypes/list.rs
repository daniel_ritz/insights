use std::{cell::RefCell, fmt::Display};

use crate::virt;

use super::{dynamic_dispatch::Method, Datatype, Object, VTable, Void};

struct List {
    subtype: Datatype,
    items: RefCell<Vec<Object>>,
}

impl List {
    pub fn type_(subtype: Datatype) -> Datatype {
        Datatype::wrapped("list", subtype, Some(vtable()))
    }

    pub fn empty(subtype: Datatype) -> Object {
        Self::new(vec![], subtype)
    }

    pub fn new(items: Vec<Object>, subtype: Datatype) -> Object {
        Object::new(
            Self::type_(subtype.clone()),
            Box::new(Self {
                subtype,
                items: RefCell::new(items),
            }),
        )
    }

    pub fn clone(&self) -> Object {
        let list = Self {
            subtype: self.subtype.clone(),
            items: self.items.clone(),
        };
        Object::new(Self::type_(self.subtype.clone()), Box::new(list))
    }

    pub fn append(&self, item: Object) -> Result<(), ()> {
        if item.type_() != &self.subtype {
            return Err(());
        }
        self.items.borrow_mut().push(item);
        Ok(())
    }

    pub fn count(&self) -> usize {
        self.items.borrow().len()
    }
}

impl PartialEq for List {
    fn eq(&self, other: &Self) -> bool {
        self.subtype == other.subtype && self.items == other.items
    }
}

impl Display for List {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let items: Vec<String> = self
            .items
            .borrow()
            .iter()
            .map(|item| item.to_string())
            .collect();
        write!(f, "[{}]", items.join(", "))
    }
}

fn vtable() -> VTable {
    VTable::builder()
        .type_method("default", Method::nullary("list[void]", default))
        .unary_method("clone", Method::unary("list[?]", clone))
        .binary_method("equals", Method::binary("list[?]", "boolean", equals))
        .unary_method("print", Method::unary("text", print))
        .binary_method("append", Method::binary("?", "boolean", append))
        .unary_method("count", Method::unary("integer", count))
        .binary_method("concat", Method::binary("list[?]", "list[?]", concat))
        .build()
}

virt! {
    default for List: List::empty(Void::type_());
    fn equals for List;
    fn clone for List;
    fn print for List;
    
    fn count(this: &List) -> Integer {
        this.count()
    }
    
    fn concat(this: &List, other: &List) -> List {
        if this.subtype != other.subtype {
            return List::empty(this.subtype.clone());
        }
        let mut list = this.items.borrow().clone();
        list.append(&mut other.items.borrow().clone());
        List::new(list, this.subtype.clone())
    }
}

fn append(this: &Object, operand: &Object) -> Object {
    let this = this.unbox::<List>();
    let result = this.append(operand.clone());
    result.is_ok().into()
}




#[cfg(test)]
mod tests {
    use crate::{
        datatypes::{Boolean, Integer, Object, Text},
        interfaces::{Collection, Concatenable},
        Interface,
    };

    use super::List;

    #[test]
    fn list_type() {
        let list = List::empty(Text::type_());
        assert_eq!("list[text]", list.type_().to_string())
    }

    #[test]
    fn print() {
        let list = List::empty(Integer::type_());
        {
            let list = list.unbox::<List>();
            list.append(Integer::new(42)).unwrap();
            list.append(Integer::new(5)).unwrap();
        }
        assert_eq!("[42, 5]", list.to_string())
    }

    #[test]
    fn cannot_append_wring_type() {
        let list = List::empty(Integer::type_());
        let list = list.unbox::<List>();
        assert!(list.append(Boolean::new(false)).is_err())
    }

    #[test]
    fn dynamic_disptach_append() {
        let list = List::empty(Integer::type_());
        assert_eq!("[]", list.to_string());
        let result = list.dispatch_binary("append", &Integer::new(42)).unwrap();
        let result = result.unbox::<Boolean>();
        assert!(result.value());
        assert_eq!("[42]", list.to_string());
    }

    #[test]
    fn equals() {
        fn prepare_list() -> Object {
            let list = List::empty(Integer::type_());
            let _ = list.unbox::<List>().append(Integer::new(42));
            list
        }

        let list1 = prepare_list();
        let list2 = prepare_list();

        assert_eq!(list1, list2)
    }

    #[test]
    fn list_is_collection() {
        let list = List::empty(Integer::type_());
        assert!(list.has_interface::<Collection>())
    }

    #[test]
    fn append_through_interface() {
        let list = List::empty(Integer::type_());
        let list = list.use_interface::<Collection>().unwrap();
        list.append(&Integer::new(42));
        let list = list.release();
        assert_eq!("[42]", list.to_string());
    }

    #[test]
    fn list_is_concatenable() {
        let list = List::empty(Integer::type_());
        assert!(list.has_interface::<Concatenable>())
    }

    fn build_list(numbers: &[i32]) -> Object {
        let list = List::empty(Integer::type_());
        let list = list.use_interface::<Collection>().unwrap();
        for num in numbers {
            list.append(&num.into());
        }
        list.release()
    }

    #[test]
    fn concat() {
        let list1 = build_list(&vec![1, 2, 3]);
        let list2 = build_list(&vec![9, 8, 7]);
        let list1 = list1.use_interface::<Concatenable>().unwrap();
        let result = list1.concat(&list2);
        assert_eq!(build_list(&vec![1, 2, 3, 9, 8, 7,]), result)
    }
}
