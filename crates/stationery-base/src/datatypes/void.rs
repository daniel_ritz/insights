use crate::virt;

use super::{dynamic_dispatch::Method, Datatype, Object, VTable};

#[derive(Debug, Default)]
pub struct Void();
impl Void {
    pub fn type_() -> Datatype {
        Datatype::basic("void", Some(vtable()))
    }
    pub fn new() -> Object {
        Object::new(Self::type_(), Box::new(Void()))
    }
}

fn vtable() -> VTable {
    VTable::builder()
        .type_method("default", Method::nullary("void", default))
        .unary_method("clone", Method::unary("void", clone))
        .binary_method("equals", Method::binary("void", "boolean", equals))
        .unary_method("print", Method::unary("text", print))
        .build()
}

virt! {
    default for Void: Void::new();
    fn equals(_x: &Void, _y: &Void) -> Boolean {
        true
    }

    fn clone(_x: &Void) -> Void {
        Void::new()
    }

    fn print(_x: &Void) -> Text {
        "<void>"
    }
}

#[cfg(test)]
mod tests {
    use crate::datatypes::Boolean;

    use super::*;

    #[test]
    fn void_dynamic_equals() {
        let void1 = Void::new();
        let void2 = Void::new();
        let result = void1
            .dispatch_binary("equals", &void2)
            .expect("'equals' method not found");
        assert!(result.unbox::<Boolean>().value());
    }

    #[test]
    fn inferred_equals() {
        let void1 = Void::new();
        let void2 = Void::new();
        assert!(void1 == void2)
    }

    #[test]
    fn inferred_not_equals() {
        let void1 = Void::new();
        let void2 = Boolean::new(true);
        assert!(void1 != void2)
    }

    #[test]
    fn dynamic_clone() {
        let first = Void::new();
        let clone = first.dispatch_unary("clone").unwrap();
        assert!(first == clone)
    }

    #[test]
    fn inferred_clone() {
        let first = Void::new();
        let clone = first.clone();
        assert!(first == clone)
    }

    #[test]
    fn print() {
        let obj = Void::new();
        assert_eq!("<void>", obj.to_string());
    }

    #[test]
    fn default() {
        assert_eq!("<void>", Void::type_().default().to_string())
    }
}
