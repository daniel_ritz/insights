use std::{
    any::Any,
    fmt::{Debug, Display},
};

use crate::{datatypes::Text, Interface};

use super::{Boolean, Datatype};

pub struct Object {
    pub(crate) datatype: Datatype,
    pub(crate) value: Box<dyn Any>,
}
impl Object {
    pub(crate) fn new(datatype: Datatype, value: Box<dyn Any>) -> Self {
        Self { datatype, value }
    }

    pub fn dispatch_unary(&self, name: &str) -> Option<Object> {
        self.datatype.dispatch_unary(name, self)
    }

    pub fn dispatch_binary(&self, name: &str, operand: &Object) -> Option<Object> {
        self.datatype.dispatch_binary(name, self, operand)
    }

    pub fn unbox<T: 'static>(&self) -> &T {
        self.value
            .downcast_ref::<T>()
            .expect("Dynamic Type did not match")
    }

    pub fn type_(&self) -> &Datatype {
        &self.datatype
    }

    pub fn has_interface<I: Interface>(&self) -> bool {
        self.type_().has_interface::<I>()
    }

    pub fn use_interface<I: Interface>(self) -> Result<I, Self> {
        if !self.has_interface::<I>() {
            Err(self)
        } else {
            Ok(I::use_(self))
        }
    }
}

impl PartialEq for Object {
    fn eq(&self, other: &Self) -> bool {
        if self.datatype != other.datatype {
            false
        } else {
            self.dispatch_binary("equals", other)
                .map_or(false, |obj| obj.unbox::<Boolean>().value())
        }
    }
}

impl Clone for Object {
    fn clone(&self) -> Self {
        self.dispatch_unary("clone").unwrap()
    }
}

impl Debug for Object {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct(&self.datatype.to_string())
            .field("value", &self.to_string())
            .finish()
    }
}

impl Display for Object {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let value = self
            .dispatch_unary("print")
            .map(|obj| obj.unbox::<Text>().value().to_string())
            .unwrap_or("<unknown>".to_string());
        write!(f, "{}", value)
    }
}
