use std::fmt::Display;

use crate::Interface;

use super::{Object, VTable, Void};

pub enum Datatype {
    Any,
    Basic {
        name: Box<String>,
        vtable: Option<VTable>,
    },
    Wrap {
        name: Box<String>,
        subtype: Box<Datatype>,
        vtable: Option<VTable>,
    },
    Struct {
        name: Box<String>,
        subtypes: Vec<(String, Datatype)>,
        vtable: Option<VTable>,
    },
}

impl Display for Datatype {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Datatype::Any => {
                write!(f, "?")
            }
            Datatype::Basic { name, vtable: _ } => {
                write!(f, "{}", name)
            }
            Datatype::Wrap {
                name,
                subtype,
                vtable: _,
            } => {
                write!(f, "{}[{}]", name, subtype)
            }
            Datatype::Struct {
                name,
                subtypes,
                vtable: _,
            } => {
                let subtypes: Vec<String> = subtypes
                    .iter()
                    .map(|subtype| format!("{}:{}", subtype.0, subtype.1))
                    .collect();
                write!(f, "{}<{}>", name, subtypes.join(","))
            }
        }
    }
}

impl Datatype {
    pub fn inflate(serial: &str) -> Datatype {
        let mut parser = DatatypeParser::new(serial);
        parser.parse()
    }

    pub fn basic(name: &str, vtable: Option<VTable>) -> Self {
        Self::Basic {
            name: Box::new(name.to_string()),
            vtable,
        }
    }

    pub fn wrapped(name: &str, subtype: Datatype, vtable: Option<VTable>) -> Self {
        Self::Wrap {
            name: Box::new(name.to_string()),
            subtype: Box::new(subtype),
            vtable,
        }
    }

    pub fn structured(name: &str, subtypes: &[(&str, Datatype)], vtable: Option<VTable>) -> Self {
        Self::Struct {
            name: Box::new(name.to_string()),
            subtypes: subtypes
                .iter()
                .map(|subtype| (subtype.0.to_string(), subtype.1.clone()))
                .collect(),
            vtable,
        }
    }

    pub(crate) fn get_dispatcher(&self) -> VTable {
        match self {
            Datatype::Any => VTable::empty(),
            Datatype::Basic { name: _, vtable } => vtable
                .as_ref()
                .cloned()
                .unwrap_or_else(VTable::empty),
            Datatype::Wrap {
                name: _,
                subtype: _,
                vtable,
            } => vtable
                .as_ref()
                .cloned()
                .unwrap_or_else(VTable::empty),
            Datatype::Struct {
                name: _,
                subtypes: _,
                vtable,
            } => vtable
                .as_ref()
                .cloned()
                .unwrap_or_else(VTable::empty),
        }
    }

    pub fn matches(&self, other: &Datatype) -> bool {
        match self {
            Datatype::Any => true,
            Datatype::Basic { name, vtable: _ } => match other {
                Datatype::Basic {
                    name: other_name,
                    vtable: _,
                } => name == other_name,
                Datatype::Any => true,
                _ => false,
            },
            Datatype::Wrap {
                name,
                subtype,
                vtable: _,
            } => match other {
                Datatype::Wrap {
                    name: other_name,
                    subtype: other_subtype,
                    vtable: _,
                } => name == other_name && subtype.matches(other_subtype),
                Datatype::Any => true,
                _ => false,
            },
            Datatype::Struct {
                name,
                subtypes,
                vtable: _,
            } => match other {
                Datatype::Struct {
                    name: other_name,
                    subtypes: other_subtypes,
                    vtable: _,
                } => name == other_name && match_all(subtypes, other_subtypes),
                Datatype::Any => true,
                _ => false,
            },
        }
    }

    pub fn dispatch_type_method(&self, name: &str) -> Option<Object> {
        self.get_dispatcher().dispatch_type_method(name)
    }

    pub fn dispatch_unary(&self, name: &str, receiver: &Object) -> Option<Object> {
        self.get_dispatcher().dispatch_unary(name, receiver)
    }
    pub fn dispatch_binary(
        &self,
        name: &str,
        receiver: &Object,
        operand: &Object,
    ) -> Option<Object> {
        self.get_dispatcher()
            .dispatch_binary(name, receiver, operand)
    }

    pub fn default(&self) -> Object {
        self.dispatch_type_method("default")
            .unwrap_or_else(Void::new)
    }

    pub fn has_interface<I: Interface>(&self) -> bool {
        self.get_dispatcher().has_interface::<I>()
    }
}

fn match_all(types: &[(String, Datatype)], other_types: &[(String, Datatype)]) -> bool {
    types
        .iter()
        .zip(other_types.iter())
        .all(|pair| pair.0 .0 == pair.1 .0 && pair.0 .1 == pair.1 .1)
}

impl PartialEq for Datatype {
    fn eq(&self, other: &Self) -> bool {
        self.matches(other)
    }
}

impl Clone for Datatype {
    fn clone(&self) -> Self {
        match self {
            Self::Any => Self::Any,
            Self::Basic { name, vtable } => Self::Basic {
                name: name.clone(),
                vtable: vtable.clone(),
            },
            Self::Wrap {
                name,
                subtype,
                vtable,
            } => Self::Wrap {
                name: name.clone(),
                subtype: subtype.clone(),
                vtable: vtable.clone(),
            },
            Self::Struct {
                name,
                subtypes,
                vtable,
            } => Self::Struct {
                name: name.clone(),
                subtypes: subtypes.clone(),
                vtable: vtable.clone(),
            },
        }
    }
}

struct DatatypeParser {
    text: String,
    position: usize,
    current_symbol: Option<char>,
}
impl DatatypeParser {
    fn new(text: &str) -> Self {
        Self {
            text: text.to_string(),
            position: 0,
            current_symbol: text.chars().nth(0),
        }
    }

    fn parse(&mut self) -> Datatype {
        match self.current_symbol {
            Some(symbol) => {
                if symbol == '?' {
                    Datatype::Any
                } else {
                    let name = self.parse_name();
                    match self.current_symbol {
                        Some('[') => self.parse_wrapped(&name),
                        Some('<') => self.parse_struct(&name),
                        _ => Datatype::basic(&name, None),
                    }
                }
            }
            None => Datatype::basic("", None),
        }
    }

    fn parse_wrapped(&mut self, name: &str) -> Datatype {
        self.advance(); // skip [
        let subtype = self.parse();
        self.advance(); // skip ]
        Datatype::wrapped(name, subtype, None)
    }

    fn parse_struct(&mut self, name: &str) -> Datatype {
        self.advance(); // skip <
        let mut subtypes = Vec::new();
        if let Some('>') = self.current_symbol {
            // struct type without subtypes
            self.advance(); // skip >
        } else {
            loop {
                let field = self.parse_field();
                subtypes.push(field);
                if let Some(symbol) = self.current_symbol {
                    if symbol != ',' {
                        break;
                    }
                    self.advance(); // skip ,
                } else {
                    break;
                }
            }

            self.advance(); // skip '>'
        }

        Datatype::Struct {
            name: Box::new(name.to_string()),
            subtypes,
            vtable: None,
        }
    }

    fn parse_field(&mut self) -> (String, Datatype) {
        let name = self.parse_name();
        if self.current_symbol.is_some_and(|symbol| symbol == ':') {
            self.advance(); // skip :
            let subtype = self.parse();
            (name, subtype)
        } else {
            ("".to_string(), Datatype::basic("", None))
        }
    }

    fn parse_name(&mut self) -> String {
        let mut name = String::new();
        while self.current_symbol.is_some() {
            let actual_symbol = self.current_symbol.unwrap();
            if !actual_symbol.is_alphanumeric() {
                break;
            }
            name.push(actual_symbol);
            self.advance();
        }
        name
    }

    fn advance(&mut self) {
        self.position += 1;
        self.current_symbol = self.text.chars().nth(self.position);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::datatypes::VTable;

    fn vtable() -> Option<VTable> {
        None
    }

    #[test]
    fn print_basic_type() {
        let int = Datatype::basic("int", vtable());
        assert_eq!("int", int.to_string());
    }

    #[test]
    fn print_wrapped_type() {
        let int = Datatype::basic("int", vtable());
        let array = Datatype::wrapped("array", int, vtable());
        assert_eq!("array[int]", array.to_string());
    }

    #[test]
    fn matches_basic_type() {
        let int1 = Datatype::basic("int", vtable());
        let int2 = Datatype::basic("int", vtable());
        assert!(int1 == int2);
    }

    #[test]
    fn mismatches_basic_type() {
        let int = Datatype::basic("int", vtable());
        let string = Datatype::basic("string", vtable());
        assert!(int != string);
    }

    #[test]
    fn matches_wrapped_type() {
        let int1 = Datatype::basic("int", vtable());
        let int2 = Datatype::basic("int", vtable());
        let array1 = Datatype::wrapped("array", int1, vtable());
        let array2 = Datatype::wrapped("array", int2, vtable());
        assert!(array1 == array2);
    }

    #[test]
    fn mismatches_wrapped_type() {
        let int1 = Datatype::basic("int", vtable());
        let int2 = Datatype::basic("int", vtable());
        let array = Datatype::wrapped("array", int1, vtable());
        let list = Datatype::wrapped("list", int2, vtable());
        assert!(array != list);
    }

    #[test]
    fn print_struct_type() {
        let int = Datatype::basic("int", vtable());
        let text = Datatype::basic("text", vtable());
        let st = Datatype::structured(
            "struct",
            &[("a", int.clone()), ("b", text.clone())],
            vtable(),
        );
        assert_eq!("struct<a:int,b:text>", st.to_string());
    }

    #[test]
    fn matches_struct_type() {
        let int = Datatype::basic("int", vtable());
        let text = Datatype::basic("text", vtable());
        let s1 = Datatype::structured(
            "struct",
            &[("a", int.clone()), ("b", text.clone())],
            vtable(),
        );
        let s2 = Datatype::structured("struct", &[("a", int), ("b", text)], vtable());
        assert!(s1 == s2)
    }

    #[test]
    fn mismatches_struct_type() {
        let int = Datatype::basic("int", vtable());
        let text = Datatype::basic("text", vtable());
        let boolean = Datatype::basic("boolean", vtable());
        let s1 = Datatype::structured("struct", &[("a", int.clone()), ("b", boolean)], vtable());
        let s2 = Datatype::structured("struct", &[("a", int), ("b", text)], vtable());
        assert!(s1 != s2)
    }

    #[test]
    fn matches_any_basic() {
        let int = Datatype::basic("int", vtable());
        assert!(int.matches(&Datatype::Any))
    }

    #[test]
    fn matches_any_in_wrapped() {
        let int = Datatype::basic("int", vtable());
        let int_list = Datatype::wrapped("list", int, vtable());
        let any_list = Datatype::wrapped("list", Datatype::Any, vtable());
        assert!(int_list.matches(&any_list))
    }

    #[test]
    fn matches_any_in_struct() {
        let int = Datatype::basic("int", vtable());
        let text = Datatype::basic("text", vtable());
        let s1 = Datatype::structured("struct", &[("a", int.clone()), ("b", text)], vtable());
        let s2 = Datatype::structured("struct", &[("a", int), ("b", Datatype::Any)], vtable());
        assert!(s1.matches(&s2))
    }

    #[test]
    fn inflate_basic() {
        let inflated = Datatype::inflate("void");
        assert_eq!("void", inflated.to_string());
    }

    #[test]
    fn inflate_any() {
        let inflated = Datatype::inflate("?");
        assert_eq!("?", inflated.to_string());
    }

    #[test]
    fn inflate_wrapped() {
        let inflated = Datatype::inflate("array[int]");
        assert_eq!("array[int]", inflated.to_string());
    }

    #[test]
    fn inflate_double_wrapped() {
        let inflated = Datatype::inflate("array[list[int]]");
        assert_eq!("array[list[int]]", inflated.to_string());
    }

    #[test]
    fn inflate_struct() {
        let inflated = Datatype::inflate("struct<a:int,b:bool>");
        assert_eq!("struct<a:int,b:bool>", inflated.to_string())
    }

    #[test]
    fn inflate_wrapped_struct() {
        let inflated = Datatype::inflate("array[struct<a:int,b:bool>]");
        assert_eq!("array[struct<a:int,b:bool>]", inflated.to_string())
    }

    #[test]
    fn inflate_double_struct() {
        let inflated = Datatype::inflate("struct<a:struct<a:int,b:bool>,b:void>");
        assert_eq!(
            "struct<a:struct<a:int,b:bool>,b:void>",
            inflated.to_string()
        )
    }

    #[test]
    fn inflate_any_wrapped() {
        let inflated = Datatype::inflate("array[?]");
        assert_eq!("array[?]", inflated.to_string())
    }

    #[test]
    fn inflate_empty_struct() {
        let inflated = Datatype::inflate("struct<>");
        assert_eq!("struct<>", inflated.to_string())
    }
}
