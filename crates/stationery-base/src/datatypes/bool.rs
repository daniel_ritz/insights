use std::fmt::Display;

use crate::virt;

use super::{dynamic_dispatch::Method, Datatype, Object, VTable};

#[derive(Debug, Eq)]
pub struct Boolean(bool);
impl Boolean {
    pub fn type_() -> Datatype {
        Datatype::basic("boolean", Some(vtable()))
    }

    pub fn new(value: bool) -> Object {
        Object::new(Self::type_(), Box::new(Boolean(value)))
    }
    pub fn value(&self) -> bool {
        self.0
    }
}
impl PartialEq for Boolean {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}

impl From<bool> for Object {
    fn from(value: bool) -> Self {
        Boolean::new(value)
    }
}

impl Display for Boolean {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", if self.value() { "yes" } else { "no" })
    }
}

fn vtable() -> VTable {
    VTable::builder()
        .type_method("default", Method::nullary("boolean", default))
        .unary_method("clone", Method::unary("boolean", clone))
        .binary_method("equals", Method::binary("boolean", "boolean", equals))
        .unary_method("print", Method::unary("text", print))
        .binary_method("and", Method::binary("boolean", "boolean", and))
        .binary_method("or", Method::binary("boolean", "boolean", or))
        .unary_method("not", Method::unary("boolean", not))
        .build()
}

virt!(
    default for Boolean: false;
    fn equals for Boolean;
    fn print for Boolean;
    fn clone(this: &Boolean) -> Boolean {
        Boolean::new(this.value())
    }
    fn and(this: &Boolean, other: &Boolean) -> Boolean {
        this.value() && other.value()
    }
    fn or(this: &Boolean, other: &Boolean) -> Boolean {
        this.value() || other.value()
    }
    fn not(this: &Boolean) -> Boolean {
        !this.value()
    }
    
);

#[cfg(test)]
mod tests {
    use crate::interfaces::Logics;

    use super::Boolean;

    #[test]
    fn create_true_bool() {
        let obj = Boolean::new(true);
        let result = obj.unbox::<Boolean>();
        assert_eq!(true, result.value())
    }

    #[test]
    fn create_false_bool() {
        let obj = Boolean::new(false);
        let result = obj.unbox::<Boolean>();
        assert_eq!(false, result.value())
    }

    #[test]
    fn bool_dynamic_equals() {
        let yes1 = Boolean::new(true);
        let yes2 = Boolean::new(true);
        let result = yes1
            .dispatch_binary("equals", &yes2)
            .expect("'equals' method not found");
        assert!(result.unbox::<Boolean>().value());
    }

    #[test]
    fn inferred_equals() {
        let yes1 = Boolean::new(true);
        let yes2 = Boolean::new(true);
        assert!(yes1 == yes2)
    }

    #[test]
    fn inferred_not_equals() {
        let yes = Boolean::new(true);
        let no = Boolean::new(false);
        assert!(yes != no)
    }

    #[test]
    fn dynamic_clone() {
        let first = Boolean::new(true);
        let clone = first.dispatch_unary("clone").unwrap();
        assert!(first == clone)
    }

    #[test]
    fn inferred_clone() {
        let first = Boolean::new(true);
        let clone = first.clone();
        assert!(first == clone)
    }

    #[test]
    fn print() {
        let obj = Boolean::new(true);
        assert_eq!("yes", obj.to_string());
    }

    #[test]
    fn default() {
        assert_eq!(false, Boolean::type_().default().unbox::<Boolean>().value())
    }

    #[test]
    fn and_true() {
        let value = Boolean::new(true);
        let value = value.use_interface::<Logics>().unwrap();
        assert_eq!(Boolean::new(true), value.and(&Boolean::new(true)))
    }
    #[test]
    fn and_false() {
        let value = Boolean::new(true);
        let value = value.use_interface::<Logics>().unwrap();
        assert_eq!(Boolean::new(false), value.and(&Boolean::new(false)))
    }

    #[test]
    fn or_true() {
        let value = Boolean::new(true);
        let value = value.use_interface::<Logics>().unwrap();
        assert_eq!(Boolean::new(true), value.or(&Boolean::new(false)))
    }
    #[test]
    fn or_false() {
        let value = Boolean::new(false);
        let value = value.use_interface::<Logics>().unwrap();
        assert_eq!(Boolean::new(false), value.or(&Boolean::new(false)))
    }

    #[test]
    fn not_true() {
        let value = Boolean::new(true);
        let value = value.use_interface::<Logics>().unwrap();
        assert_eq!(Boolean::new(false), value.not())
    }
    #[test]
    fn not_false() {
        let value = Boolean::new(false);
        let value = value.use_interface::<Logics>().unwrap();
        assert_eq!(Boolean::new(true), value.not())
    }
}
