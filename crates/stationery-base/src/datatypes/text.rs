use std::fmt::{write, Display};

use crate::virt;

use super::{dynamic_dispatch::Method, Datatype, Object, VTable};

#[derive(Debug, Eq, Default)]
pub struct Text {
    value: String,
}
impl Text {
    pub fn type_() -> Datatype {
        Datatype::basic("text", Some(vtable()))
    }

    pub fn value(&self) -> &str {
        &self.value
    }

    pub fn new(value: &str) -> Object {
        Object::new(
            Self::type_(),
            Box::new(Self {
                value: value.to_string(),
            }),
        )
    }

    pub fn clone(&self) -> Object {
        Self::new(&self.value)
    }
}
impl PartialEq for Text {
    fn eq(&self, other: &Self) -> bool {
        self.value == other.value
    }
}
impl Display for Text {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl From<&str> for Object {
    fn from(value: &str) -> Self {
        Text::new(value)
    }
}

impl From<String> for Object {
    fn from(value: String) -> Self {
        Text::new(&value)
    }
}

fn vtable() -> VTable {
    VTable::builder()
        .type_method("default", Method::nullary("text", default))
        .unary_method("clone", Method::unary("text", clone))
        .binary_method("equals", Method::binary("text", "boolean", equals))
        .unary_method("print", Method::unary("text", print))
        .unary_method("count", Method::unary("integer", count))
        .binary_method("concat", Method::binary("text", "text", concat))
        .build()
}

virt! {
    default for Text: "";
    fn equals for Text;
    fn clone for Text;
    fn print for Text;

    fn count(this: &Text) -> Integer {
        this.value().len()
    }

    fn concat(this: &Text, other: &Text) -> Text {
        this.value.to_string() + &other.value
    }
}

#[cfg(test)]
mod tests {
    use crate::interfaces::Concatenable;

    use super::*;

    #[test]
    fn new_text() {
        let text = Text::new("hello, world");
        assert_eq!("hello, world", text.unbox::<Text>().value());
    }

    #[test]
    fn equals() {
        let t1 = Text::new("hello");
        let t2 = Text::new("hello");
        assert_eq!(t1, t2);
    }

    #[test]
    fn not_equals() {
        let t1 = Text::new("hello");
        let t2 = Text::new("world");
        assert_ne!(t1, t2);
    }

    #[test]
    fn clone() {
        let text = Text::new("hello");
        let clone = text.clone();
        assert_eq!(text, clone);
    }

    #[test]
    fn print() {
        let text = Text::new("hello");
        assert_eq!("hello", text.to_string());
    }

    #[test]
    fn default() {
        assert_eq!("", Text::type_().default().to_string())
    }

    #[test]
    fn count() {
        let text = Text::new("hello");
        let text = text.use_interface::<Concatenable>().unwrap();
        assert_eq!(5, text.count());
    }

    #[test]
    fn concat() {
        let hello = Text::new("hello");
        let world = Text::new("world");
        let hello = hello.use_interface::<Concatenable>().unwrap();
        let full = hello.concat(&world);
        assert_eq!(Text::new("helloworld"), full);
    }
}
