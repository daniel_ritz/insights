use std::collections::HashMap;

use crate::{datatypes::Datatype, Interface, Object};

use super::{BinaryFunction, Method, MethodList, NullaryFunction, UnaryFunction};

pub struct VTable {
    type_methods: HashMap<String, MethodList<NullaryFunction>>,
    unary_methods: HashMap<String, MethodList<UnaryFunction>>,
    binary_methods: HashMap<String, MethodList<BinaryFunction>>,
}

impl VTable {
    pub fn empty() -> Self {
        Self {
            type_methods: HashMap::new(),
            unary_methods: HashMap::new(),
            binary_methods: HashMap::new(),
        }
    }
    pub fn new(
        binary_methods: HashMap<String, MethodList<BinaryFunction>>,
        unary_methods: HashMap<String, MethodList<UnaryFunction>>,
        type_methods: HashMap<String, MethodList<NullaryFunction>>,
    ) -> Self {
        Self {
            type_methods,
            unary_methods,
            binary_methods,
        }
    }

    pub fn builder() -> VTableBuilder {
        VTableBuilder::new()
    }

    pub fn dispatch_type_method(&self, name: &str) -> Option<Object> {
        self.type_methods
            .get(name)
            .and_then(|method_list| method_list.find(&Datatype::Any))
            .map(|method| method.invoke())
    }

    pub fn dispatch_unary(&self, name: &str, data: &Object) -> Option<Object> {
        self.unary_methods
            .get(name)
            .and_then(|method_list| method_list.find(&Datatype::Any))
            .map(|method| method.invoke(data))
    }

    pub fn dispatch_binary(&self, name: &str, lhs: &Object, rhs: &Object) -> Option<Object> {
        self.binary_methods
            .get(name)
            .and_then(|method_list| method_list.find(rhs.type_()))
            .map(|method| method.invoke(lhs, rhs))
    }

    pub fn has_interface<I: Interface>(&self) -> bool {
        let interface = I::required_methods();
        let fulfills_unary = interface
            .unary_methods
            .iter()
            .all(|method| self.has_unary_method(method));
        let fulfills_binary = interface
            .binary_methods
            .iter()
            .all(|method| self.has_binary_method(method));

        fulfills_unary && fulfills_binary
    }

    fn has_unary_method(&self, name: &str) -> bool {
        self.unary_methods.keys().any(|method| method == name)
    }

    fn has_binary_method(&self, name: &str) -> bool {
        self.binary_methods.keys().any(|method| method == name)
    }
}

impl Clone for VTable {
    fn clone(&self) -> Self {
        Self {
            type_methods: self.type_methods.clone(),
            unary_methods: self.unary_methods.clone(),
            binary_methods: self.binary_methods.clone(),
        }
    }
}

pub struct VTableBuilder {
    type_methods: HashMap<String, MethodList<NullaryFunction>>,
    unary_methods: HashMap<String, MethodList<UnaryFunction>>,
    binary_methods: HashMap<String, MethodList<BinaryFunction>>,
}
impl VTableBuilder {
    pub fn new() -> Self {
        Self {
            type_methods: HashMap::new(),
            unary_methods: HashMap::new(),
            binary_methods: HashMap::new(),
        }
    }

    pub fn type_method(mut self, name: &str, method: Method<NullaryFunction>) -> Self {
        if !self.type_methods.contains_key(name) {
            self.type_methods
                .insert(name.to_string(), MethodList::new());
        }
        if let Some(list) = self.type_methods.get_mut(name) {
            list.register(method)
        }
        self
    }

    pub fn unary_method(mut self, name: &str, method: Method<UnaryFunction>) -> Self {
        if !self.unary_methods.contains_key(name) {
            self.unary_methods
                .insert(name.to_string(), MethodList::new());
        }
        if let Some(list) = self.unary_methods.get_mut(name) {
            list.register(method)
        }
        self
    }

    pub fn binary_method(mut self, name: &str, method: Method<BinaryFunction>) -> Self {
        if !self.binary_methods.contains_key(name) {
            self.binary_methods
                .insert(name.to_string(), MethodList::new());
        }
        if let Some(list) = self.binary_methods.get_mut(name) {
            list.register(method)
        }
        self
    }

    pub fn build(self) -> VTable {
        VTable::new(self.binary_methods, self.unary_methods, self.type_methods)
    }
}

#[cfg(test)]
mod tests {
    use crate::datatypes::{Boolean, Datatype, Integer, Void};

    use super::*;

    #[test]
    fn unary_dispatch() {
        let dispatcher = VTable::builder()
            .unary_method("hello", Method::unary("boolean", |_| Boolean::new(true)))
            .build();
        let result = dispatcher.dispatch_unary("hello", &Void::new());
        assert!(result.is_some() && result.unwrap().unbox::<Boolean>().value() == true)
    }

    #[test]
    fn unary_dispatch_not_found() {
        let dispatcher = VTable::builder()
            .unary_method("hello", Method::unary("boolean", |_| Boolean::new(true)))
            .build();
        let result = dispatcher.dispatch_unary("foo", &Void::new());
        assert!(result.is_none())
    }

    #[test]
    fn binary_dispatch() {
        let dispatcher = VTable::builder()
            .binary_method(
                "hello",
                Method::binary("void", "boolean", |_, _| Boolean::new(true)),
            )
            .build();
        let result = dispatcher.dispatch_binary("hello", &Void::new(), &Void::new());
        assert!(result.is_some() && result.unwrap().unbox::<Boolean>().value() == true)
    }

    #[test]
    fn binary_dispatch_not_found() {
        let dispatcher = VTable::builder()
            .binary_method("hello", Method::binary("void", "void", |_, _| Void::new()))
            .build();
        let result = dispatcher.dispatch_binary("foo", &Void::new(), &Void::new());
        assert!(result.is_none())
    }

    #[test]
    fn type_dispatch() {
        let dispatcher = VTable::builder()
            .type_method("hello", Method::nullary("void", || Void::new()))
            .build();
        let result = dispatcher.dispatch_type_method("hello");
        assert!(result.is_some())
    }

    #[test]
    fn new_binary_dispatch() {
        let dispatcher = VTable::builder()
            .binary_method(
                "hello",
                Method {
                    func: |_, _| Integer::new(1),
                    operand_type: Integer::type_(),
                    return_type: Integer::type_(),
                },
            )
            .binary_method(
                "hello",
                Method {
                    func: |_, _| Integer::new(2),
                    operand_type: Boolean::type_(),
                    return_type: Integer::type_(),
                },
            )
            .build();
        let result = dispatcher
            .dispatch_binary("hello", &Void::new(), &Integer::new(0))
            .unwrap();
        assert_eq!(Integer::new(1), result)
    }

    #[test]
    fn new_binary_dispatch_any() {
        let dispatcher = VTable::builder()
            .binary_method(
                "hello",
                Method {
                    func: |_, _| Integer::new(1),
                    operand_type: Integer::type_(),
                    return_type: Integer::type_(),
                },
            )
            .binary_method(
                "hello",
                Method {
                    func: |_, _| Integer::new(2),
                    operand_type: Datatype::Any,
                    return_type: Integer::type_(),
                },
            )
            .build();
        let result = dispatcher
            .dispatch_binary("hello", &Void::new(), &Integer::new(0))
            .unwrap();
        assert_eq!(Integer::new(2), result)
    }
}
