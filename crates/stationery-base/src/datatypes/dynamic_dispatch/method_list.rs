use std::fmt::Display;

use crate::datatypes::Datatype;

use super::{BinaryFunction, Method, NullaryFunction, UnaryFunction};

#[derive(Clone)]
pub struct MethodList<T: Clone> {
    methods: Vec<Method<T>>,
}
impl<T: Clone> MethodList<T> {
    pub fn new() -> Self {
        Self { methods: vec![] }
    }
    pub fn find(&self, operand_type: &Datatype) -> Option<Method<T>> {
        self.methods
            .iter()
            .rev() // The overwriting methods were appended at the end
            .find(|method| method.operand_type.matches(operand_type))
            .cloned()
    }
}

impl<T: Clone> Display for MethodList<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let methods: Vec<String> = self
            .methods
            .iter()
            .map(|method| method.to_string())
            .collect();
        write!(f, "[{}]", methods.join(","))
    }
}

impl MethodList<NullaryFunction> {
    pub fn register(&mut self, method: Method<NullaryFunction>) {
        self.methods.push(method);
    }
}

impl MethodList<UnaryFunction> {
    pub fn register(&mut self, method: Method<UnaryFunction>) {
        self.methods.push(method);
    }
}

impl MethodList<BinaryFunction> {
    pub fn register(&mut self, method: Method<BinaryFunction>) {
        self.methods.push(method);
    }
}

#[cfg(test)]
mod tests {
    use crate::datatypes::{Boolean, Integer, Void};

    use super::*;

    fn build_method_list() -> MethodList<BinaryFunction> {
        let mut methods: MethodList<BinaryFunction> = MethodList::new();
        methods.register(Method {
            func: |_, _| Integer::new(1),
            operand_type: Integer::type_(),
            return_type: Integer::type_(),
        });
        methods.register(Method {
            func: |_, _| Integer::new(2),
            operand_type: Integer::type_(),
            return_type: Integer::type_(),
        });
        methods.register(Method {
            func: |_, _| Integer::new(3),
            operand_type: Boolean::type_(),
            return_type: Integer::type_(),
        });
        return methods;
    }

    #[test]
    fn find_latest_method() {
        let methods = build_method_list();
        let method = methods.find(&Integer::type_()).unwrap();
        let result = (method.func)(&Void::new(), &Integer::new(0));
        assert_eq!(Integer::new(2), result);
    }

    #[test]
    fn find_method() {
        let methods = build_method_list();
        let method = methods.find(&Boolean::type_()).unwrap();
        let result = (method.func)(&Void::new(), &Boolean::new(false));
        assert_eq!(Integer::new(3), result);
    }

    #[test]
    fn find_unknown_method() {
        let methods = build_method_list();
        let method = methods.find(&Void::type_());
        assert!(method.is_none());
    }

    #[test]
    fn find_any_method() {
        let methods = build_method_list();
        let method = methods.find(&Datatype::Any).unwrap();
        let result = (method.func)(&Void::new(), &Void::new());
        assert_eq!(Integer::new(3), result);
    }
}
