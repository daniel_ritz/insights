use std::fmt::Display;

use crate::{datatypes::Datatype, Object};

pub(crate) type NullaryFunction = fn() -> Object;
pub(crate) type UnaryFunction = fn(&Object) -> Object;
pub(crate) type BinaryFunction = fn(&Object, &Object) -> Object;

#[derive(Clone)]
pub struct Method<T: Sized> {
    pub func: T,
    pub operand_type: Datatype,
    pub return_type: Datatype,
}

impl<T: Sized> Display for Method<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "? x {} -> {}", self.operand_type, self.return_type)
    }
}
impl Method<BinaryFunction> {
    pub fn binary(operand_type: &str, return_type: &str, func: BinaryFunction) -> Self {
        Method::<BinaryFunction> {
            func,
            operand_type: Datatype::inflate(operand_type),
            return_type: Datatype::inflate(return_type),
        }
    }
    pub fn invoke(&self, receiver: &Object, operand: &Object) -> Object {
        (self.func)(receiver, operand)
    }
}
impl Method<UnaryFunction> {
    pub fn unary(return_type: &str, func: UnaryFunction) -> Self {
        Method::<UnaryFunction> {
            func,
            operand_type: Datatype::Any,
            return_type: Datatype::inflate(return_type),
        }
    }
    pub fn invoke(&self, receiver: &Object) -> Object {
        (self.func)(receiver)
    }
}
impl Method<NullaryFunction> {
    pub fn nullary(return_type: &str, func: NullaryFunction) -> Self {
        Method::<NullaryFunction> {
            func,
            operand_type: Datatype::Any,
            return_type: Datatype::inflate(return_type),
        }
    }
    pub fn invoke(&self) -> Object {
        (self.func)()
    }
}

#[macro_export]
macro_rules! virt {
    () => {};
    (
        fn $name:ident(
            $receiver:ident: &$receiver_type:ty,
            $operand:ident: &$operand_type:ty
        ) -> $return_type:ty $code:block
        $($tail:tt)*
    ) => {
        fn $name($receiver: &Object, $operand: &Object) -> Object {
            let $receiver = $receiver.unbox::<$receiver_type>();
            let $operand = $operand.unbox::<$operand_type>();
            let result = $code;
            result.into()
        }
        virt!($($tail)*);
    };
    (
        fn $name:ident(
            $receiver:ident: &$receiver_type:ty
        ) -> $return_type:ty $code:block
        $($tail:tt)*
    ) => {
        fn $name($receiver: &Object) -> Object {
            let $receiver = $receiver.unbox::<$receiver_type>();
            let result = $code;
            result.into()
        }
        virt!($($tail)*);
    };
    (
        fn $name:ident() -> $return_type:ty $code:block
        $($tail:tt)*
    ) => {
        fn $name() -> Object {
            $code.into()
        }
        virt!($($tail)*);
    };
    (
        fn equals for $type:ty;
        $($tail:tt)*
    ) => {
        fn equals(this: &Object, other: &Object) -> Object {
            let this = this.unbox::<$type>();
            let other = other.unbox::<$type>();
            (this == other).into()
        }
        virt!($($tail)*);
    };
    (
        fn clone for $type:ty;
        $($tail:tt)*
    ) => {
        fn clone(this: &Object) -> Object {
            let this = this.unbox::<$type>();
            this.clone().into()
        }
        virt!($($tail)*);
    };
    (
        fn print for $type:ty;
        $($tail:tt)*
    ) => {
        fn print(this: &Object) -> Object {
            let this = this.unbox::<$type>();
            this.to_string().into()
        }
        virt!($($tail)*);
    };
    (
        default for $type:ty: $default:expr;
        $($tail:tt)*
    ) => {
        fn default() -> Object {
            $default.into()
        }
        virt!($($tail)*);
    };
}

#[cfg(test)]
mod tests {
    use crate::datatypes::{Boolean, Void};

    use super::Method;

    #[test]
    fn binary_method() {
        let method = Method::binary("boolean", "boolean", |_, _| Boolean::new(true));
        assert_eq!(
            Boolean::new(true),
            (method.func)(&Void::new(), &Boolean::new(false))
        );
    }
}
