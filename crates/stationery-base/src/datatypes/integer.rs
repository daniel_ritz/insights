use std::fmt::Display;

use crate::virt;

use super::{dynamic_dispatch::Method, Datatype, Object, VTable};

#[derive(Debug, Default)]
pub struct Integer {
    value: i64,
}

impl Integer {
    pub fn type_() -> Datatype {
        Datatype::basic("integer", Some(vtable()))
    }
    pub fn new(value: i64) -> Object {
        Object::new(Self::type_(), Box::new(Self { value }))
    }
    pub fn value(&self) -> i64 {
        self.value
    }
}

impl Display for Integer {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl PartialEq for Integer {
    fn eq(&self, other: &Self) -> bool {
        self.value == other.value
    }
}

impl From<&i64> for Object {
    fn from(value: &i64) -> Self {
        Integer::new(*value)
    }
}

impl From<i64> for Object {
    fn from(value: i64) -> Self {
        Integer::new(value)
    }
}

impl From<&i32> for Object {
    fn from(value: &i32) -> Self {
        Integer::new(*value as i64)
    }
}

impl From<usize> for Object {
    fn from(value: usize) -> Self {
        Integer::new(value as i64)
    }
}

fn vtable() -> VTable {
    VTable::builder()
        .type_method("default", Method::nullary("integer", default))
        .unary_method("clone", Method::unary("integer", clone))
        .binary_method("equals", Method::binary("integer", "boolean", equals))
        .unary_method("print", Method::unary("text", print))
        .binary_method("add", Method::binary("integer", "integer", add))
        .binary_method("subtract", Method::binary("integer", "integer", subtract))
        .binary_method("multiply", Method::binary("integer", "integer", multiply))
        .binary_method("divide", Method::binary("integer", "integer", divide))
        .build()
}

virt! {
    default for Integer: &0;
    fn equals for Integer;
    fn print for Integer;
    fn clone(this: &Integer) -> Integer {
        Integer::new(this.value())
    }

    fn add(lhs: &Integer, rhs: &Integer) -> Integer {
        lhs.value() + rhs.value()
    }
    
    fn subtract(lhs: &Integer, rhs: &Integer) -> Integer {
        lhs.value() - rhs.value()
    }
    
    fn multiply(lhs: &Integer, rhs: &Integer) -> Integer {
        lhs.value() * rhs.value()
    }
    
    fn divide(lhs: &Integer, rhs: &Integer) -> Integer {
        lhs.value() / rhs.value()
    }
}






#[cfg(test)]
mod tests {
    use crate::interfaces::Arithmetics;

    use super::*;

    #[test]
    fn new_integer() {
        let int = Integer::new(42);
        assert_eq!(42, int.unbox::<Integer>().value());
    }

    #[test]
    fn integer_equals() {
        let i1 = Integer::new(42);
        let i2 = Integer::new(42);
        assert_eq!(i1, i2);
    }

    #[test]
    fn integer_not_equals() {
        let i1 = Integer::new(42);
        let i2 = Integer::new(0);
        assert_ne!(i1, i2);
    }

    #[test]
    fn clone() {
        let int = Integer::new(42);
        let clone = int.clone();
        assert_eq!(int, clone);
    }

    #[test]
    fn print() {
        let obj = Integer::new(42);
        assert_eq!("42", obj.to_string());
    }

    #[test]
    fn default() {
        assert_eq!(0, Integer::type_().default().unbox::<Integer>().value())
    }

    #[test]
    fn add() {
        let num = Integer::new(42);
        let num = num.use_interface::<Arithmetics>().unwrap();
        let result = num.add(&Integer::new(5));
        assert_eq!(Integer::new(47), result)
    }

    #[test]
    fn subtract() {
        let num = Integer::new(42);
        let num = num.use_interface::<Arithmetics>().unwrap();
        let result = num.subtract(&Integer::new(5));
        assert_eq!(Integer::new(37), result)
    }

    #[test]
    fn multiply() {
        let num = Integer::new(42);
        let num = num.use_interface::<Arithmetics>().unwrap();
        let result = num.multiply(&Integer::new(2));
        assert_eq!(Integer::new(84), result)
    }

    #[test]
    fn divide() {
        let num = Integer::new(42);
        let num = num.use_interface::<Arithmetics>().unwrap();
        let result = num.divide(&Integer::new(2));
        assert_eq!(Integer::new(21), result)
    }
}
