use crate::Object;

pub trait Interface {
    fn required_methods() -> InterfaceDeclaration;
    fn use_(object: Object) -> Self;
    fn release(self) -> Object;
}

pub struct InterfaceDeclaration {
    pub unary_methods: Vec<&'static str>,
    pub binary_methods: Vec<&'static str>,
}
